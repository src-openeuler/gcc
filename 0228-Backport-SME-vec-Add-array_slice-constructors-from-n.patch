From 044dc671f7eb723df5b6ce2364d6ae579c0cc984 Mon Sep 17 00:00:00 2001
From: Martin Jambor <mjambor@suse.cz>
Date: Tue, 30 Aug 2022 18:50:35 +0200
Subject: [PATCH 129/157] [Backport][SME] vec: Add array_slice constructors
 from non-const and gc vectors

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=15433c214df295f2281a90fcf283355b21beca0e

This patch adds constructors of array_slice that are required to
create them from non-const (heap or auto) vectors or from GC vectors.

gcc/ChangeLog:

2022-08-08  Martin Jambor  <mjambor@suse.cz>

	* vec.h (array_slice): Add constructors for non-const reference to
	heap vector and pointers to heap vectors.
---
 gcc/vec.h | 12 ++++++++++++
 1 file changed, 12 insertions(+)

diff --git a/gcc/vec.h b/gcc/vec.h
index 3ba7ea7ed..fc3b10c85 100644
--- a/gcc/vec.h
+++ b/gcc/vec.h
@@ -2264,6 +2264,18 @@ public:
   array_slice (const vec<OtherT> &v)
     : m_base (v.address ()), m_size (v.length ()) {}
 
+  template<typename OtherT>
+  array_slice (vec<OtherT> &v)
+    : m_base (v.address ()), m_size (v.length ()) {}
+
+  template<typename OtherT>
+  array_slice (const vec<OtherT, va_gc> *v)
+    : m_base (v ? v->address () : nullptr), m_size (v ? v->length () : 0) {}
+
+  template<typename OtherT>
+  array_slice (vec<OtherT, va_gc> *v)
+    : m_base (v ? v->address () : nullptr), m_size (v ? v->length () : 0) {}
+
   iterator begin () { return m_base; }
   iterator end () { return m_base + m_size; }
 
-- 
2.33.0

