From 2ae587a86bba31b91a127e353c31c9f861ff5326 Mon Sep 17 00:00:00 2001
From: Xi Ruoyao <xry111@xry111.site>
Date: Tue, 8 Nov 2022 13:42:20 +0800
Subject: [PATCH 030/124] LoongArch: Add flogb.{s,d} instructions and expand
 logb{sf,df}2

On LoongArch, flogb instructions extract the exponent of a non-negative
floating point value, but produces NaN for negative values.  So we need
to add a fabs instruction when we expand logb.

gcc/ChangeLog:

	* config/loongarch/loongarch.md (UNSPEC_FLOGB): New unspec.
	(type): Add flogb.
	(logb_non_negative<mode>2): New instruction template.
	(logb<mode>2): New define_expand.

gcc/testsuite/ChangeLog:

	* gcc.target/loongarch/flogb.c: New test.

Signed-off-by: Peng Fan <fanpeng@loongson.cn>
Signed-off-by: ticat_fp <fanpeng@loongson.cn>
---
 gcc/config/loongarch/loongarch.md          | 35 ++++++++++++++++++++--
 gcc/testsuite/gcc.target/loongarch/flogb.c | 18 +++++++++++
 2 files changed, 51 insertions(+), 2 deletions(-)
 create mode 100644 gcc/testsuite/gcc.target/loongarch/flogb.c

diff --git a/gcc/config/loongarch/loongarch.md b/gcc/config/loongarch/loongarch.md
index c141c9add..682ab9617 100644
--- a/gcc/config/loongarch/loongarch.md
+++ b/gcc/config/loongarch/loongarch.md
@@ -42,6 +42,7 @@
   UNSPEC_FTINTRM
   UNSPEC_FTINTRP
   UNSPEC_FSCALEB
+  UNSPEC_FLOGB
 
   ;; Override return address for exception handling.
   UNSPEC_EH_RETURN
@@ -217,6 +218,7 @@
 ;; fdiv		floating point divide
 ;; frdiv	floating point reciprocal divide
 ;; fabs		floating point absolute value
+;; flogb	floating point exponent extract
 ;; fneg		floating point negation
 ;; fcmp		floating point compare
 ;; fcopysign	floating point copysign
@@ -233,8 +235,8 @@
   "unknown,branch,jump,call,load,fpload,fpidxload,store,fpstore,fpidxstore,
    prefetch,prefetchx,condmove,mgtf,mftg,const,arith,logical,
    shift,slt,signext,clz,trap,imul,idiv,move,
-   fmove,fadd,fmul,fmadd,fdiv,frdiv,fabs,fneg,fcmp,fcopysign,fcvt,fscaleb,
-   fsqrt,frsqrt,accext,accmod,multi,atomic,syncloop,nop,ghost"
+   fmove,fadd,fmul,fmadd,fdiv,frdiv,fabs,flogb,fneg,fcmp,fcopysign,fcvt,
+   fscaleb,fsqrt,frsqrt,accext,accmod,multi,atomic,syncloop,nop,ghost"
   (cond [(eq_attr "jirl" "!unset") (const_string "call")
 	 (eq_attr "got" "load") (const_string "load")
 
@@ -1039,6 +1041,35 @@
    (set_attr "mode" "<UNITMODE>")])
 
 ;;
+;;  ....................
+;;
+;;	FLOATING POINT EXPONENT EXTRACT
+;;
+;;  ....................
+
+(define_insn "logb_non_negative<mode>2"
+  [(set (match_operand:ANYF 0 "register_operand" "=f")
+	(unspec:ANYF [(match_operand:ANYF 1 "register_operand" "f")]
+		     UNSPEC_FLOGB))]
+  "TARGET_HARD_FLOAT"
+  "flogb.<fmt>\t%0,%1"
+  [(set_attr "type" "flogb")
+   (set_attr "mode" "<UNITMODE>")])
+
+(define_expand "logb<mode>2"
+  [(set (match_operand:ANYF 0 "register_operand")
+	(unspec:ANYF [(abs:ANYF (match_operand:ANYF 1 "register_operand"))]
+		     UNSPEC_FLOGB))]
+  "TARGET_HARD_FLOAT"
+{
+  rtx tmp = gen_reg_rtx (<MODE>mode);
+
+  emit_insn (gen_abs<mode>2 (tmp, operands[1]));
+  emit_insn (gen_logb_non_negative<mode>2 (operands[0], tmp));
+  DONE;
+})
+
+;;
 ;;  ...................
 ;;
 ;;  Count leading zeroes.
diff --git a/gcc/testsuite/gcc.target/loongarch/flogb.c b/gcc/testsuite/gcc.target/loongarch/flogb.c
new file mode 100644
index 000000000..1daefe54e
--- /dev/null
+++ b/gcc/testsuite/gcc.target/loongarch/flogb.c
@@ -0,0 +1,18 @@
+/* { dg-do compile } */
+/* { dg-options "-mdouble-float -fno-math-errno" } */
+/* { dg-final { scan-assembler "fabs\\.s" } } */
+/* { dg-final { scan-assembler "fabs\\.d" } } */
+/* { dg-final { scan-assembler "flogb\\.s" } } */
+/* { dg-final { scan-assembler "flogb\\.d" } } */
+
+double
+my_logb (double a)
+{
+  return __builtin_logb (a);
+}
+
+float
+my_logbf (float a)
+{
+  return __builtin_logbf (a);
+}
-- 
2.33.0

