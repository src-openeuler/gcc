From eb92c185c1c71edcbd83b1c66fe4f9e7d52a98b3 Mon Sep 17 00:00:00 2001
From: Richard Sandiford <richard.sandiford@arm.com>
Date: Thu, 29 Sep 2022 11:32:56 +0100
Subject: [PATCH 024/157] [Backport][SME] aarch64: Tweak contents of
 flags_on/off fields

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=bb7f43b62a58a0f0326fd3060f0bd43e6f3ef971

After previous changes, it's more convenient if the flags_on and
flags_off fields of all_extensions include the feature flag itself.

gcc/
	* common/config/aarch64/aarch64-common.cc (all_extensions):
	Include the feature flag in flags_on and flags_off.
	(aarch64_parse_extension): Update accordingly.
	(aarch64_get_extension_string_for_isa_flags): Likewise.
---
 gcc/common/config/aarch64/aarch64-common.cc | 14 ++++++--------
 1 file changed, 6 insertions(+), 8 deletions(-)

diff --git a/gcc/common/config/aarch64/aarch64-common.cc b/gcc/common/config/aarch64/aarch64-common.cc
index 3efa57b26..752ba5632 100644
--- a/gcc/common/config/aarch64/aarch64-common.cc
+++ b/gcc/common/config/aarch64/aarch64-common.cc
@@ -213,10 +213,8 @@ struct aarch64_option_extension
 static constexpr aarch64_option_extension all_extensions[] =
 {
 #define AARCH64_OPT_EXTENSION(NAME, IDENT, C, D, E, F) \
-  {NAME, AARCH64_FL_##IDENT, \
-   feature_deps::IDENT ().explicit_on & ~AARCH64_FL_##IDENT, \
-   feature_deps::get_flags_off (feature_deps::root_off_##IDENT) \
-   & ~AARCH64_FL_##IDENT},
+  {NAME, AARCH64_FL_##IDENT, feature_deps::IDENT ().explicit_on, \
+   feature_deps::get_flags_off (feature_deps::root_off_##IDENT)},
 #include "config/aarch64/aarch64-option-extensions.def"
   {NULL, 0, 0, 0}
 };
@@ -304,9 +302,9 @@ aarch64_parse_extension (const char *str, aarch64_feature_flags *isa_flags,
 	    {
 	      /* Add or remove the extension.  */
 	      if (adding_ext)
-		*isa_flags |= (opt->flags_on | opt->flag_canonical);
+		*isa_flags |= opt->flags_on;
 	      else
-		*isa_flags &= ~(opt->flags_off | opt->flag_canonical);
+		*isa_flags &= ~opt->flags_off;
 	      break;
 	    }
 	}
@@ -380,7 +378,7 @@ aarch64_get_extension_string_for_isa_flags
 
       if ((flags & isa_flags & (explicit_flags | ~current_flags)) == flags)
 	{
-	  current_flags |= opt.flag_canonical | opt.flags_on;
+	  current_flags |= opt.flags_on;
 	  added |= opt.flag_canonical;
 	}
     }
@@ -395,7 +393,7 @@ aarch64_get_extension_string_for_isa_flags
   for (auto &opt : all_extensions)
     if (opt.flag_canonical & current_flags & ~isa_flags)
       {
-	current_flags &= ~(opt.flag_canonical | opt.flags_off);
+	current_flags &= ~opt.flags_off;
 	outstr += "+no";
 	outstr += opt.name;
       }
-- 
2.33.0

