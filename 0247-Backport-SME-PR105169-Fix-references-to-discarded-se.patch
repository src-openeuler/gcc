From 6cc7bcadadbc2521a2db4c02adfe066d805e37ef Mon Sep 17 00:00:00 2001
From: Giuliano Belinassi <gbelinassi@suse.de>
Date: Fri, 6 May 2022 23:37:52 -0300
Subject: [PATCH 148/157] [Backport][SME] PR105169 Fix references to discarded
 sections

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=7a3f38a966a52893fb5bae301a1a3d56961358fb

When -fpatchable-function-entry= is enabled, certain C++ codes fails to
link because of generated references to discarded sections in
__patchable_function_entry section. This commit fixes this problem by
puting those references in a COMDAT section.

2022-05-06  Giuliano Belinassi  <gbelinassi@suse.de>

gcc/ChangeLog
	PR c++/105169
	* targhooks.cc (default_print_patchable_function_entry_1): Handle COMDAT case.
	* varasm.cc (switch_to_comdat_section): New
	(handle_vtv_comdat_section): Call switch_to_comdat_section.
	* varasm.h: Declare switch_to_comdat_section.

gcc/testsuite/ChangeLog
2022-05-06  Giuliano Belinassi  <gbelinassi@suse.de>

	PR c++/105169
	* g++.dg/modules/pr105169.h: New file.
	* g++.dg/modules/pr105169_a.C: New test.
	* g++.dg/modules/pr105169_b.C: New file.
---
 gcc/targhooks.cc                          |  8 ++++--
 gcc/testsuite/g++.dg/modules/pr105169.h   | 22 +++++++++++++++
 gcc/testsuite/g++.dg/modules/pr105169_a.C | 25 +++++++++++++++++
 gcc/testsuite/g++.dg/modules/pr105169_b.C | 12 +++++++++
 gcc/varasm.cc                             | 33 ++++++++++++++---------
 gcc/varasm.h                              |  2 ++
 6 files changed, 87 insertions(+), 15 deletions(-)
 create mode 100644 gcc/testsuite/g++.dg/modules/pr105169.h
 create mode 100644 gcc/testsuite/g++.dg/modules/pr105169_a.C
 create mode 100644 gcc/testsuite/g++.dg/modules/pr105169_b.C

diff --git a/gcc/targhooks.cc b/gcc/targhooks.cc
index c88afa5db..175a0e18a 100644
--- a/gcc/targhooks.cc
+++ b/gcc/targhooks.cc
@@ -2019,8 +2019,12 @@ default_print_patchable_function_entry_1 (FILE *file,
       patch_area_number++;
       ASM_GENERATE_INTERNAL_LABEL (buf, "LPFE", patch_area_number);
 
-      switch_to_section (get_section ("__patchable_function_entries",
-				      flags, current_function_decl));
+      section *sect = get_section ("__patchable_function_entries",
+				  flags, current_function_decl);
+      if (HAVE_COMDAT_GROUP && DECL_COMDAT_GROUP (current_function_decl))
+	switch_to_comdat_section (sect, current_function_decl);
+      else
+	switch_to_section (sect);
       assemble_align (POINTER_SIZE);
       fputs (asm_op, file);
       assemble_name_raw (file, buf);
diff --git a/gcc/testsuite/g++.dg/modules/pr105169.h b/gcc/testsuite/g++.dg/modules/pr105169.h
new file mode 100644
index 000000000..a7e762705
--- /dev/null
+++ b/gcc/testsuite/g++.dg/modules/pr105169.h
@@ -0,0 +1,22 @@
+class IPXAddressClass
+{
+public:
+    IPXAddressClass(void);
+};
+
+class WinsockInterfaceClass
+{
+
+public:
+    WinsockInterfaceClass(void);
+
+    virtual void Set_Broadcast_Address(void*){};
+
+    virtual int Get_Protocol(void)
+    {
+        return 0;
+    };
+
+protected:
+};
+
diff --git a/gcc/testsuite/g++.dg/modules/pr105169_a.C b/gcc/testsuite/g++.dg/modules/pr105169_a.C
new file mode 100644
index 000000000..66dc4b790
--- /dev/null
+++ b/gcc/testsuite/g++.dg/modules/pr105169_a.C
@@ -0,0 +1,25 @@
+/* { dg-module-do link } */
+/* { dg-options "-std=c++11 -fpatchable-function-entry=1 -O2" } */
+/* { dg-additional-options "-std=c++11 -fpatchable-function-entry=1 -O2" } */
+
+/* This test is in the "modules" package because it supports multiple files
+   linkage.  */
+
+#include "pr105169.h"
+
+WinsockInterfaceClass* PacketTransport;
+
+IPXAddressClass::IPXAddressClass(void)
+{
+}
+
+int function()
+{
+  return PacketTransport->Get_Protocol();
+}
+
+int main()
+{
+  IPXAddressClass ipxaddr;
+  return 0;
+}
diff --git a/gcc/testsuite/g++.dg/modules/pr105169_b.C b/gcc/testsuite/g++.dg/modules/pr105169_b.C
new file mode 100644
index 000000000..5f8b00dfe
--- /dev/null
+++ b/gcc/testsuite/g++.dg/modules/pr105169_b.C
@@ -0,0 +1,12 @@
+/* { dg-module-do link } */
+/* { dg-options "-std=c++11 -fpatchable-function-entry=1 -O2" } */
+/* { dg-additional-options "-std=c++11 -fpatchable-function-entry=1 -O2" } */
+
+/* This test is in the "modules" package because it supports multiple files
+   linkage.  */
+
+#include "pr105169.h"
+
+WinsockInterfaceClass::WinsockInterfaceClass(void)
+{
+}
diff --git a/gcc/varasm.cc b/gcc/varasm.cc
index 3f69b47a7..bae935694 100644
--- a/gcc/varasm.cc
+++ b/gcc/varasm.cc
@@ -8459,25 +8459,21 @@ default_asm_output_ident_directive (const char *ident_str)
     fprintf (asm_out_file, "%s\"%s\"\n", ident_asm_op, ident_str);
 }
 
-
-/* This function ensures that vtable_map variables are not only
-   in the comdat section, but that each variable has its own unique
-   comdat name.  Without this the variables end up in the same section
-   with a single comdat name.
-
+/* Switch to a COMDAT section with COMDAT name of decl.
+   
    FIXME:  resolve_unique_section needs to deal better with
    decls with both DECL_SECTION_NAME and DECL_ONE_ONLY.  Once
    that is fixed, this if-else statement can be replaced with
    a single call to "switch_to_section (sect)".  */
 
-static void
-handle_vtv_comdat_section (section *sect, const_tree decl ATTRIBUTE_UNUSED)
+void
+switch_to_comdat_section (section *sect, tree decl)
 {
 #if defined (OBJECT_FORMAT_ELF)
   targetm.asm_out.named_section (sect->named.name,
 				 sect->named.common.flags
 				 | SECTION_LINKONCE,
-				 DECL_NAME (decl));
+				 decl);
   in_section = sect;
 #else
   /* Neither OBJECT_FORMAT_PE, nor OBJECT_FORMAT_COFF is set here.
@@ -8492,18 +8488,18 @@ handle_vtv_comdat_section (section *sect, const_tree decl ATTRIBUTE_UNUSED)
     {
       char *name;
 
-      if (TREE_CODE (DECL_NAME (decl)) == IDENTIFIER_NODE)
+      if (TREE_CODE (decl) == IDENTIFIER_NODE)
 	name = ACONCAT ((sect->named.name, "$",
-			 IDENTIFIER_POINTER (DECL_NAME (decl)), NULL));
+			 IDENTIFIER_POINTER (decl), NULL));
       else
 	name = ACONCAT ((sect->named.name, "$",
-			 IDENTIFIER_POINTER (DECL_COMDAT_GROUP (DECL_NAME (decl))),
+			 IDENTIFIER_POINTER (DECL_COMDAT_GROUP (decl)),
 			 NULL));
 
       targetm.asm_out.named_section (name,
 				     sect->named.common.flags
 				     | SECTION_LINKONCE,
-				     DECL_NAME (decl));
+				     decl);
       in_section = sect;
     }
   else
@@ -8511,4 +8507,15 @@ handle_vtv_comdat_section (section *sect, const_tree decl ATTRIBUTE_UNUSED)
 #endif
 }
 
+/* This function ensures that vtable_map variables are not only
+   in the comdat section, but that each variable has its own unique
+   comdat name.  Without this the variables end up in the same section
+   with a single comdat name.  */
+
+static void
+handle_vtv_comdat_section (section *sect, const_tree decl ATTRIBUTE_UNUSED)
+{
+  switch_to_comdat_section(sect, DECL_NAME (decl));
+}
+
 #include "gt-varasm.h"
diff --git a/gcc/varasm.h b/gcc/varasm.h
index d5d8c4e55..8ba8374e7 100644
--- a/gcc/varasm.h
+++ b/gcc/varasm.h
@@ -79,4 +79,6 @@ extern rtx assemble_static_space (unsigned HOST_WIDE_INT);
 
 extern rtx assemble_trampoline_template (void);
 
+extern void switch_to_comdat_section (section *, tree);
+
 #endif  // GCC_VARASM_H
-- 
2.33.0

