From 54a6e52207703a8643fc406175377105f887ebef Mon Sep 17 00:00:00 2001
From: Richard Sandiford <richard.sandiford@arm.com>
Date: Tue, 12 Sep 2023 16:05:04 +0100
Subject: [PATCH] [Backport][SME] aarch64: Avoid a use of callee_offset

Reference: https://gcc.gnu.org/git/?p=gcc.git;a=commit;h=f9ab771fa8cd747f34786c6f33deea32c2eb828b

When we emit the frame chain, i.e. when we reach Here in this statement
of aarch64_expand_prologue:

  if (emit_frame_chain)
    {
      // Here
      ...
    }

the stack is in one of two states:

- We've allocated up to the frame chain, but no more.

- We've allocated the whole frame, and the frame chain is within easy
  reach of the new SP.

The offset of the frame chain from the current SP is available
in aarch64_frame as callee_offset.  It is also available as the
chain_offset local variable, where the latter is calculated from other
data.  (However, chain_offset is not always equal to callee_offset when
!emit_frame_chain, so chain_offset isn't redundant.)

In c600df9a4060da3c6121ff4d0b93f179eafd69d1 I switched to using
chain_offset for the initialisation of the hard frame pointer:

       aarch64_add_offset (Pmode, hard_frame_pointer_rtx,
-                         stack_pointer_rtx, callee_offset,
+                         stack_pointer_rtx, chain_offset,
                          tmp1_rtx, tmp0_rtx, frame_pointer_needed);

But the later REG_CFA_ADJUST_CFA handling still used callee_offset.

I think the difference is harmless, but it's more logical for the
CFA note to be in sync, and it's more convenient for later patches
if it uses chain_offset.

gcc/
	* config/aarch64/aarch64.cc (aarch64_expand_prologue): Use
	chain_offset rather than callee_offset.
---
 gcc/config/aarch64/aarch64.cc | 4 +---
 1 file changed, 1 insertion(+), 3 deletions(-)

diff --git a/gcc/config/aarch64/aarch64.cc b/gcc/config/aarch64/aarch64.cc
index b7da1d0be..fbd7a079a 100644
--- a/gcc/config/aarch64/aarch64.cc
+++ b/gcc/config/aarch64/aarch64.cc
@@ -10263,21 +10263,20 @@ aarch64_epilogue_uses (int regno)
    current FP is also set up if it is in use.  */
 
 void
 aarch64_expand_prologue (void)
 {
   aarch64_frame &frame = cfun->machine->frame;
   poly_int64 frame_size = frame.frame_size;
   poly_int64 initial_adjust = frame.initial_adjust;
   HOST_WIDE_INT callee_adjust = frame.callee_adjust;
   poly_int64 final_adjust = frame.final_adjust;
-  poly_int64 callee_offset = frame.callee_offset;
   poly_int64 sve_callee_adjust = frame.sve_callee_adjust;
   poly_int64 below_hard_fp_saved_regs_size
     = frame.below_hard_fp_saved_regs_size;
   unsigned reg1 = frame.wb_push_candidate1;
   unsigned reg2 = frame.wb_push_candidate2;
   bool emit_frame_chain = frame.emit_frame_chain;
   rtx_insn *insn;
 
   if (flag_stack_clash_protection && known_eq (callee_adjust, 0))
     {
@@ -10376,22 +10375,21 @@ aarch64_expand_prologue (void)
 	     the CFA based on the frame pointer.  We therefore need new
 	     DW_CFA_expressions to re-express the save slots with addresses
 	     based on the frame pointer.  */
 	  rtx_insn *insn = get_last_insn ();
 	  gcc_assert (RTX_FRAME_RELATED_P (insn));
 
 	  /* Add an explicit CFA definition if this was previously
 	     implicit.  */
 	  if (!find_reg_note (insn, REG_CFA_ADJUST_CFA, NULL_RTX))
 	    {
-	      rtx src = plus_constant (Pmode, stack_pointer_rtx,
-				       callee_offset);
+	      rtx src = plus_constant (Pmode, stack_pointer_rtx, chain_offset);
 	      add_reg_note (insn, REG_CFA_ADJUST_CFA,
 			    gen_rtx_SET (hard_frame_pointer_rtx, src));
 	    }
 
 	  /* Change the save slot expressions for the registers that
 	     we've already saved.  */
 	  aarch64_add_cfa_expression (insn, regno_reg_rtx[reg2],
 				      hard_frame_pointer_rtx, UNITS_PER_WORD);
 	  aarch64_add_cfa_expression (insn, regno_reg_rtx[reg1],
 				      hard_frame_pointer_rtx, 0);
-- 
2.38.1.windows.1

